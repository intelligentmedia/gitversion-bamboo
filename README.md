Learn more about using [GitVersion for Bamboo](http://carolynvanslyck.com/projects/gitversion/).

Use the [GitVersion](http://particularlabs.github.io/GitVersion/) [semantic versioning](http://semver.org/) tool to set a version number for your builds and deployments.
